from django.db import models
# Create yo
# ur models here.
class ProductMainModel(models.Model):
    Title=models.CharField(max_length=300)
    Description=models.CharField(max_length=300)
    Unique_id:models.URLField()
    Price:models.IntegerField()

class ProductImageModel(models.Model):
    product=models.ForeignKey(ProductMainModel,on_delete=models.CASCADE)
    image=models.ImageField()

class User(models.Model):
    import pdb;pdb.set_trace()
    Phone_number=models.IntegerField()
    Email=models.EmailField(max_length=30)
    is_customer=models.BooleanField()
    is_admin=models.BooleanField()
    
class UserProfile(models.Model):
    gender=[
        ("M",'Male'),
        ('F','Female'),
        ("O",'Others')
    ]
    owner=models.OneToOneField(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100)
    Date_of_birth=models.DateTimeField()
    Gender=models.CharField(max_length=1,choices=gender)
    image=models.ImageField()

class UserLogIn(models.Model):
    owner=models.ForeignKey(User,on_delete=models.CASCADE)
    otp=models.ImageField()
    active=models.BooleanField()
    def __str__(self):
        return self.owner,self.otp,self.active
    

    
class UserCartProduct(models.Model):
    owner=models.ForeignKey(User,on_delete=models.CASCADE)
    status=models.Choices('pending','completed')
    product=models.ForeignKey(ProductMainModel,on_delete=models.CASCADE)

class UserCartModel(models.Model):
    owner=models.OneToOneField(User,on_delete=models.CASCADE)
    products=models.OneToOneField(UserCartProduct,on_delete=models.CASCADE)
    Price=models.IntegerField()
	