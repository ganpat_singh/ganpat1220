from django.shortcuts import render
from django.views import View
from . models import *
from django.http import HttpResponse,JsonResponse,BadHeaderError,HttpResponseRedirect
from django.core.mail import send_mail
# Create your views here.
# 1.create user(Phone_number,Name,Date_of_birth,Gender,Image), Create user cart while creating user(post request)
# 2.get all users(get request)

class Userdata(View):
    def get(self,request):
        try:
            data=User.objects.all()
            return JsonResponse(data)
        except:
            return JsonResponse([])
    def post(self,request):
        import pdb;pdb.set_trace()
        try:
            choice=[
                ('M','Male'),
                ('F','FEMALE'),
                ('O','OTHERS')]
            status_choice=[
                ("p",'Pending'),
                ('c','complete')
            ]
            data=request.POST
            user= User(Phone_number=data.get("Phone_number"),Email=data.get('Email'),is_customer=data.get('is_customer'),is_admin=data.get('is_admin'))
            user.save()
            user_prof=UserProfile(owner=user,name=data.get('name'),Date_of_birth=data.get('Date_of_birth'),Gender=dict(choice)[data.get('Gender')],image=data.get('image',""))
            user_prof.save()
            cart=UserCartModel(owner=user,products=UserCartProduct(),Price=data.get('Price'))
            cart()
            return JsonResponse(status=200,data={'user':user,'profile':user_prof,'cart':cart})
        except:
            return JsonResponse(status=400,message='Something went wrong')
        
   # UserCartProduct(owner=user,status=dict(status_choice)[data.get('status')],product=ProductMainModel(Title=data.get('Title'),Description=data.get("Description"),Unique_id=models.BigAutoField(),Price=data.get('Price'))


class CreateProduct(View):
    def get(self,request):
        data=ProductMainModel.objects.all()
        return JsonResponse(data)
    def post(self,request):
        data=request.POST
        pro=ProductMainModel(Title=data.get('Title'),Description=data.get('Description'),Price=data.get('Price'))
        pro.save()
        return HttpResponse("Product is created")

class Cretemulimage(View):
    def get(self,request):
        data=ProductImageModel.objects.all()
        return JsonResponse(data)
    def post(self,request):
        data=request.POST
        pro=ProductMainModel(Title=data.get('Title'),Description=data.get('Description'),Price=data.get('Price'))
        pro.save()
        image=request.Files
        product = ProductImageModel(product=pro,image=image)
        product.save()
        return JsonResponse(product)


class SendEmail(View):
    def post(self,request):
        otp = request.POST.get('otp', '')
        message = request.POST.get('message', '')
        from_email = request.POST.get('from_email', '')
        if otp and message and from_email:
            try:
                send_mail(otp, message, from_email, ['admin@example.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('/contact/thanks/')
        else:
            return HttpResponse('Make sure all fields are entered and valid.')
        

class LogIn(View):
    def post(self,request):
        data=request.POST
        user=UserLogIn(owner=None,otp=data.get('otp'),active=data.get('active'))
        user.save()
        return HttpResponse('User successfully login')
